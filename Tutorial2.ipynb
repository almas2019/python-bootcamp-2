{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "d5c5b247",
   "metadata": {},
   "source": [
    "# Python Tutorial 2\n",
    "## MBP1201H Introduction to Biostatistics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "5da76f04",
   "metadata": {},
   "outputs": [],
   "source": [
    "from statsmodels.distributions.empirical_distribution import ECDF\n",
    "from scipy.stats import norm, kstest, pearsonr, ttest_ind, zscore\n",
    "from statsmodels.api import qqplot\n",
    "import numpy as np\n",
    "from sklearn.linear_model import LinearRegression\n",
    "from functools import partial"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55aaaea4",
   "metadata": {},
   "source": [
    "## Problem 1 - Normality Tests\n",
    "Many statistical tests require the input distributions to be normal, or follow a Gaussian probability density function. We're going to investigate a couple of tests to determine if an arbitrary distribution can be considered normal. For this problem, we'll use the `np.random.normal()` function to generate a synthetic dataset `X` with mean 5 and standard deviation 1 from a normal distribution. \n",
    "\n",
    "### Part 1A - QQ Plots\n",
    "Plot a histogram of X. Does it look visually normal? Use the `qqplot()` function to generate a Quantile-Quantile plot of X. Do the points align well with the identity line $y=x$?\n",
    "\n",
    "### Part 1B - Kolmogorov-Smirnov Test\n",
    "\n",
    "The Kolmogorov-Smirnov test compares two distributions $A$ and $B$ to determine if they are statistically the same distribution. In this case, we will use it to determine if a random distribution is normally distributed. What is the KS statistic of X with respect to the normal CDF `norm.cdf`? Is this statistically significant at the level $\\alpha=0.05$? *Note*: I've provided a function `CDF` below that shifts `norm.cdf` by the mean of X and scales it by the standard deviation of `X`. \n",
    "\n",
    "### Part 1C - Mathematical Intuition for KS Statistic\n",
    "Show that the KS statistic above is equal to (numerically within 1e-3 is fine)\n",
    "$$ D = \\max_{x \\in X}|\\mathrm{ECDF}(x) - \\mathrm{CDF}(x)|$$\n",
    "using the `ECDF` function from statsmodels and the `CDF` function from scipy.stats. *Hint*: `np.max()` computes the maximum value of an array, and `np.abs()` computes the absolute value. \n",
    "\n",
    "If you have time, plot the ECDF and CDF on the same axes along with a vertical line connecting the ECDF and CDF at the point where they are maximally different. *Hint*: `np.argmax()` computes the index where the maximum value occurs.\n",
    "\n",
    "### Part 1D - Central Limit Theorem and Sample Size\n",
    "The central limit theorem says that if we sample multiple averages from any probability distribution, it will eventually look like the normal distribution. Let's put this to the test using the `np.random.poisson()` function. Suppose that we want to take 1000 averages of size $S$. How large should $S$ be to approximate a normal distribution at the level $\\alpha=0.05$? For simplicity, we will only consider sizes that are multiples of 5.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "4efac12b",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(2)\n",
    "\n",
    "X = np.random.normal(loc=5, scale=1, size=1000)\n",
    "\n",
    "CDF = partial(norm.cdf, loc=5, scale=1)\n",
    "\n",
    "# Usage:\n",
    "# X = np.linspace(0, 1, 1000)\n",
    "# CDF_x, CDF_y = CDF(X)\n",
    "\n",
    "\n",
    "Y = np.random.poisson(lam=5.0, size=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acb99c86",
   "metadata": {},
   "source": [
    "## Problem 2 - Linear Analysis\n",
    "Often when comparing two measurements of a single quantity, we're interested in the linear relationship between these measurements, the degree of correlation, and any bias observed between the two measurements. We'll introduce some tools to quantify linear relationships between two measurements. For this problem, we'll use the dataset `myocardial_mass.csv` that contains measurements of myocardial mass in grams by cardiac MRI from two observers O1 and O2.\n",
    "\n",
    "### Part 2A - Linear Regression\n",
    "What is the slope of best fit? Intercept of best fit? Overplot the data and line of best fit on the same axes. \n",
    "\n",
    "### Part 2B - Pearson $r$ Correlation\n",
    "Pearson $r$ ($\\in [-1, 1]$) denotes the degree of positive or negative linear correlation between two measurements. If $r > 0$, there is positive correlation. If $r < 0$, there is negative correlation. 0 denotes no correlation, and $\\pm$1 denotes maximal linear correlation. What is the Pearson $r$ correlation between O1 and O2? Is this significant at the level $\\alpha = 0.05$?\n",
    "\n",
    "### Part 2C - Bland-Altman Analysis\n",
    "Bland-Altman plots are useful for characterizing bias between two measurements at multiple value scales. Make a Bland-Altman plot for O1 and O2 using the following steps.\n",
    "\n",
    "* Compute A = (O1 + O2)/2\n",
    "* Compute B = O2 - O1\n",
    "* Compute the mean $\\mu$ and standard deviation $\\sigma$ of B\n",
    "* Plot A on the x axis and B on the y axis\n",
    "* Plot horizontal lines at y=0 and y=$\\mu$. If the data has minimal mean bias, $\\mu$ should be close to 0.\n",
    "* Plot horizontal lines at y=$\\mu \\pm 1.96\\sigma$. These are the limits of agreement and determine the variability between O1 and O2. \n",
    "\n",
    "What is the mean bias and the limits of agreement? What can you interpret from the plot?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "12b7f0d8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/javascript": [
       "\n",
       "        if (window._pyforest_update_imports_cell) { window._pyforest_update_imports_cell('import pandas as pd'); }\n",
       "    "
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mass = pd.read_csv('myocardial_mass.csv')\n",
    "\n",
    "O1 = mass[\"O1\"].values.reshape(-1, 1)\n",
    "O2 = mass[\"O2\"].values.reshape(-1, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aef3313c",
   "metadata": {},
   "source": [
    "## Problem 3 - Basic Statistical Tests\n",
    "Here, we introduce some basic statistical tests to determine if two distributions are statistically significant from each other. For this problem, we'll be looking at differences in effective temperature (measured in Kelvin) of two types of stars: Type 1 stars host planets, and Type 2 stars are the control population. \n",
    "\n",
    "### Part 3A - Z Score\n",
    "Compute the Z score for each of the stars. What is the average Z score? Plot a histogram of the data and the z scores. \n",
    "\n",
    "### Part 3B- $t$ Test\n",
    "\n",
    "Using a student's $t$-test (`ttest_ind`), determine if there is statistical significance between Type 1 stars and Type 2 stars at the level $\\alpha=0.05$. Do the assumptions of the $t$-test hold for these distributions?\n",
    "\n",
    "\n",
    "### Part 3C - One-tailed vs Two-tailed Tests\n",
    "In the `ttest_ind` function, try passing in different values ('less', 'greater', 'two-sided') for the `alternative` flag. Which of these flags make the most sense?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "48430b30",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/javascript": [
       "\n",
       "        if (window._pyforest_update_imports_cell) { window._pyforest_update_imports_cell('import pandas as pd'); }\n",
       "    "
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "data = np.genfromtxt('censor.dat', skip_header=1)\n",
    "\n",
    "df = pd.DataFrame()\n",
    "df[\"StarType\"] = data[:, 1]\n",
    "df[\"EffTemp\"] = data[:, 2]\n",
    "\n",
    "type_1 = df.loc[df[\"StarType\"] == 1.][\"EffTemp\"]\n",
    "type_2 = df.loc[df[\"StarType\"] == 2.][\"EffTemp\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eab2c476",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
